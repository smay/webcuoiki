import { Injectable } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  user: Observable<firebase.User>;

  constructor(private auth: AngularFireAuth, private router: Router) {
    this.user = this.auth.authState;
  }

  signup(user) {
    this.auth.auth.createUserWithEmailAndPassword(user.email, user.password)
    .then(u => {
      console.log(u);
    })
    .catch(err => console.log(err));
  }

  signin(user) {
    this.auth.auth.signInWithEmailAndPassword(user.email, user.password)
    .then(u => {
      this.router.navigate(['/']);
    })
    .catch(err => console.log(err));
  }

  getCurrentUser() {
    return this.user;
  }

  signout() {
    this.auth.auth.signOut();
  }
}
