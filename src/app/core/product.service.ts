import { Injectable } from '@angular/core';
import { AngularFireDatabase, AngularFireList } from 'angularfire2/database';
import { FileUpload } from '../model/fileupload';
import * as firebase from 'firebase';
import { Product } from '../model/product';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  constructor(private db: AngularFireDatabase) { }

  pushFileToStorage(fileUpload: FileUpload) {
    const storageRef = firebase.storage().ref();
    const uploadTask = storageRef.child(`products/images/${fileUpload.name}`).put(fileUpload.file);

    uploadTask.on(firebase.storage.TaskEvent.STATE_CHANGED,
      (snapshot) => {
        const snap = snapshot as firebase.storage.UploadTaskSnapshot;
      }, error => console.log(error), () => {
        uploadTask.snapshot.ref.getDownloadURL().then(downloadURL => {
          console.log('File available at', downloadURL);
          fileUpload.url = downloadURL;
          console.log(downloadURL);
          this.saveFileData(fileUpload);
        });
      }
    );
  }

  private saveFileData(fileUpload: FileUpload) {
    console.log(fileUpload.name);
    this.db.list(`products`).push(fileUpload);
  }

  getListProducts(): AngularFireList<Product[]> {
    return this.db.list('/products');
  }

  getListProductsTop() {
    return this.db.list('/products').query.orderByChild('price').limitToFirst(6);
  }
}
