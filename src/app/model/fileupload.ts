export class FileUpload {

    key: string;
    name: string;
    url: string;
    file: File;
    title: string;
    price: number;
    description: string;

    constructor(title: string, price: number, description: string, file: File) {
        this.name = title.split(' ').join('_');
        this.title = title;
        this.price = price;
        this.description = description;
        this.file = file;
    }
}
