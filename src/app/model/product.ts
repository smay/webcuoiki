export class Product {
    url: string;
    title: string;
    price: number;
    description: string;
}
