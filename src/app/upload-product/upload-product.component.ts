import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from 'angularfire2/firestore';
import { ProductService } from '../core/product.service';
import { FileUpload } from '../model/fileupload';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-upload-product',
  templateUrl: './upload-product.component.html',
  styleUrls: ['./upload-product.component.css']
})
export class UploadProductComponent implements OnInit {

  product: FormGroup;
  selectedFiles: FileList;
  currentFileUpload: FileUpload;

  constructor(private productService: ProductService, private fb: FormBuilder) { }

  ngOnInit() {
    this.buildForm();
  }

  buildForm() {
    this.product = this.fb.group({
      'title': ['', Validators.required],
      'price': ['', Validators.required],
      'description': ['', Validators.required],
    });
  }

  selectFile(event) {
    this.selectedFiles = event.target.files;
    console.log(event.target.files);
  }

  upload() {
    const file = this.selectedFiles.item(0);
    this.selectedFiles = undefined;

    this.currentFileUpload = new FileUpload(
      this.product.value['title'],
      this.product.value['price'],
      this.product.value['description'],
      file);
    this.productService.pushFileToStorage(this.currentFileUpload);
  }
}
