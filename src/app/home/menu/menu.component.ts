import { Component, OnInit } from '@angular/core';
import { ProductService } from 'src/app/core/product.service';
import { config } from 'rxjs';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {
  constructor(private productService: ProductService) { }

  products = [];
  options = [];
  searchs = [];
  p = 1;
  config = {
    displayKey: 'title',
    search: true,
    height: 'auto',
    placeholder: 'Select',
    limitTo: 5,
    moreText: 'more',
    noResultsFound: 'No results found!',
    searchPlaceholder: 'Search',
    searchOnKey: 'name',
  };

  ngOnInit() {
    this.productService.getListProducts().valueChanges().subscribe(value => {
      this.products = value;
      this.options = value;
    });
  }

  changeValue(event) {
    if (this.searchs.length) {
      this.products = this.searchs;
    } else {
      this.products = this.options;
    }
  }
}
