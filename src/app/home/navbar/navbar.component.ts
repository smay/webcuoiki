import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { AuthService } from 'src/app/core/auth.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  emailCurrentUser = null;

  constructor(private auth: AuthService) { }

  ngOnInit() {
    this.auth.getCurrentUser().subscribe(user => {
      if (user) {
        this.emailCurrentUser = user.email;
      }
    });
  }

  logout() {
    this.auth.signout();
    console.log('logout');
  }

}
