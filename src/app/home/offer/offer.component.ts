import { Component, OnInit } from '@angular/core';
import { ProductService } from 'src/app/core/product.service';
import { AuthService } from 'src/app/core/auth.service';

@Component({
  selector: 'app-offer',
  templateUrl: './offer.component.html',
  styleUrls: ['./offer.component.css']
})
export class OfferComponent implements OnInit {

  p1 = 1;
  products = [];
  topProducts = [];
  user = null;

  constructor(private productService: ProductService, private authService: AuthService) { }

  ngOnInit() {
    this.productService.getListProducts().valueChanges().subscribe(value => {
      this.products = value;
      this.topProducts = [];
      this.productService.getListProductsTop().once('value', snapshot => {
        snapshot.forEach(data => { this.topProducts.push(data.val()); });
      });
    });
    this.authService.getCurrentUser().subscribe(user => this.user = user);
  }

}
