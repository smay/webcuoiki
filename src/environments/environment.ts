// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  config: {
    apiKey: 'AIzaSyC_8xqCWr4ipWoxbZciJgu4ocshH1-P658',
    authDomain: 'practiceweb-cebc2.firebaseapp.com',
    databaseURL: 'https://practiceweb-cebc2.firebaseio.com',
    projectId: 'practiceweb-cebc2',
    storageBucket: 'practiceweb-cebc2.appspot.com',
    messagingSenderId: '632612145375'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
